let mongoose = require('mongoose')
const { Schema } = mongoose;

const resetPasswordSchema = new Schema(
    {
        otp: {
            type: String,
            trim: true,
            required: true
        },
        email: {
            type: String,
            trim: true,
            lowercase: true
        },
        mobile: {
            type: String,
            trim: true,
        }
    },
    { timestamps: true }
);

module.exports = resetPassword = mongoose.model('resetPassword', resetPasswordSchema);
