const User = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const AWS = require("aws-sdk");
const { promisify } = require("util");

const questions = require("../models/questions");
const CareerDepartments = require('../models/careerDepartments');
const courses = require('../models/courses');
const subCourses = require('../models/subcourse');

const {
  submitTestValidation
} = require("../validations/question/submitTestValidation");

//  signup
module.exports = {
  getAllQuestions: async (req, res) => {
    try {
      let data = await questions.find({});

      return res.json({
        data: data,
        error: false,
      });
    } catch (err) {
      return res.json({
        error: true,
        message: "something wnt wrong",
      });
    }
  },

  getActiveQuestions: async (req, res) => {
    try {
      let data = await questions.find({ isActive: true });
      console.log(data);

      let response = {}

      return res.json({
        data: data,
        error: false,
      });
    } catch (err) {
      return res.json({
        error: true,
        message: "something went wrong",
        data: err + "",
      });
    }
  },

  addNewQuestion: async (req, res) => {
    try {
      console.log('question body___:', req.body);
      // saving new question
      const qns = new questions({
        question: req.body.fieldValues.question,
        optionA: {
          option: req.body.fieldValues.a,
          departmentId: req.body.fieldValues.departmentA
        },
        optionB: {
          option: req.body.fieldValues.b,
          departmentId: req.body.fieldValues.departmentB
        },
        optionC: {
          option: req.body.fieldValues.c,
          departmentId: req.body.fieldValues.departmentC
        },
        optionD: {
          option: req.body.fieldValues.d,
          departmentId: req.body.fieldValues.departmentD
        },
        comments: req.body.fieldValues.comments,
      });

      let saved = await qns.save();

      return res.json({
        error: false,
        data: saved,
      });

    } catch (err) {
      return res.json({
        error: true,
        message: "something wnt wrong",
        data: err + "",
      });
    }
  },

  activateOrDeactivateQuestion: async (req, res) => {
    try {
      if (!req.body.id) {
        return res.json({
          error: true,
          message: "id and isActive is required",
        });
      }

      let question = await questions.findOne({ _id: req.body.id });

      if (!question) {
        return res.json({
          error: true,
          message: "Invalid id",
        });
      }

      let message = "";
      if (question.isActive) {
        question.isActive = false;
        message = 'Question deactivated';
      } else {
        question.isActive = true;
        message = 'Question activated';
      }

      await questions.updateOne({ _id: req.body.id }, { isActive: question.isActive });

      return res.json({
        error: false,
        message
      });
    } catch (err) {
      return res.json({
        error: true,
        message: "something wnt wrong",
        data: err + "",
      });
    }
  },

  submitTest: async (req, res) => {
    try {
      // body validation
      console.log('submit test body:', req.body);
      const dataValidation = await submitTestValidation(req.body);

      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, "");
        return res.json({
          error: true,
          message: message,
        });
      }

      let answers = req.body.answers;


      // fetching all active questions
      let allQuestions = await questions.find({ isActive: true });

      let departments = {};


      // finding correct answers and increasing those department's count
      answers.forEach((answer) => {
        let question = allQuestions.find(q => q._id == answer.question_id);
        if (question) {
          console.log('question_found:', question);
          let departmentOfThisQuestion = ''

          if (answer.answer == 'A') {
            departmentOfThisQuestion = question.optionA.departmentId
          } else if (answer.answer == 'B') {
            departmentOfThisQuestion = question.optionB.departmentId
          } else if (answer.answer == 'C') {
            departmentOfThisQuestion = question.optionC.departmentId
          } else if (answer.answer == 'D') {
            departmentOfThisQuestion = question.optionD.departmentId
          }

          console.log('departmentOfThisQuestion:', departmentOfThisQuestion);

          if (departments[departmentOfThisQuestion]) {
            departments[departmentOfThisQuestion] += 1;
          } else {
            departments[departmentOfThisQuestion] = 1;
          }

        }
      });


      // finding most interested department
      let interestArr = Object.values(departments);
      let maxInterest = Math.max(...interestArr);

      function getKeyByValue(object, value) {
        return Object.keys(object).find(key => object[key] === value);
      }

      let interestedDepartment = getKeyByValue(departments, maxInterest)

      let departmentDetails = await CareerDepartments.findOneAndUpdate({ _id: interestedDepartment }, { $inc: { interestedCount: 1 } });


      // adding the interested department to the user
      await User.updateOne(
        { _id: req.user._id },
        {
          $set: {
            interestedCareerDepartment: departmentDetails._id,
            interestedCareerDepartmentName: departmentDetails.departmentName,
          },
        }
      );

      let message = `You are most interested in ${departmentDetails.departmentName} category`;

      let course360Details = await courses.findOne({is360: true});

      let starterVideoSubCourse = await subCourses.findOne({subCourseName: 'Conventional Careers'});
      let starterVideoDetails = starterVideoSubCourse.videoList.find(video => video.videoName == 'Management');
      
      let videoId = '';
      let videoName = '';

      if (starterVideoDetails) {
        videoId = starterVideoDetails.videoId;
        videoName = starterVideoDetails.videoName;
      }

      return res.json({
        error: false,
        message: "Test submitted successfully",
        data: {
          message,
          course360Id: course360Details._id,
          videoId,
          videoName,
        },
      });
    } catch (err) {
      return res.json({
        error: true,
        message: "something wnt wrong",
        data: err + "",
      });
    }
  },

  // skipping 360 degree aptitude test
  skipTest: async (req, res) => {
    try {
      console.log('skip test body:', req.user._id);
      await User.updateOne(
        { _id: req.user._id },
        {
          $set: {
            isSkippedTest: true,
          },
        }
      );

      let course360Details = await courses.findOne({ is360: true });

      return res.json({
        error: false,
        message: "Test skipped successfully",
        data: {
          course360Id: course360Details._id,
        }
      });

    } catch (err) {
      return res.json({
        error: true,
        message: "something wnt wrong",
        data: err + "",
      });
    }
  },
      

};
