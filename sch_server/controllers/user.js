const User = require("../models/user");
const resetPassword = require('../models/resetPassword')
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const AWS = require('aws-sdk')
const FileValidation = require('../helpers/FileValidation')
const fs = require('fs');
const { promisify } = require('util')
const unlinkAsync = promisify(fs.unlink)
const { resetPasswordTemplate } = require('../email/resetpassword')
const axios = require('axios')

const {
  signupValidation,
  loginValidation,
  profileUpdateValidation,
  validateGetResetOtp,
  validateResetPassword,
} = require("../validations/user/userValidator");

const aws_config = {
  ses: {
    accessKeyId: process.env.IAM_ACCESS_KEY,
    secretAccessKey: process.env.IAM_SECRET_KEY,
    region: 'ap-south-1',
    apiVersion: '2012-10-17'
  }
}
const ses = new AWS.SES(aws_config.ses)

// email regex validator
function validateEmail(emailAdress) {
  let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (emailAdress.match(regexEmail)) {
    return true;
  } else {
    return false;
  }
}

// mobile number regex validator
function validateMobileNumber(number) {
  if (/^\d{10}$/.test(number)) {
    return true;
  } else {
    return false
  }
}

//  signup
module.exports = {
  login: async (req, res) => {
    let params = req.body;

    // data validation
    const dataValidation = await loginValidation(params);

    if (dataValidation.error) {
      const message = dataValidation.error.details[0].message.replace(/"/g, "");
      console.log(message);
      return res.json({
        error: true,
        data: "",
        message: message,
      });
    }

    try {
      let user = await User.findOne({ email: req.body.email });
      if (user) {
        // compare passwrd
        if (user.isBlocked == false) {
          let passwordMatch = await bcrypt.compare(
            req.body.password,
            user.password
          );

          if (passwordMatch) {
            // create signed jwt
            const token = await jwt.sign(
              { _id: user._id },
              process.env.JWT_SECRET,
              {
                expiresIn: "30d",
              }
            );
            // send token in cookie
            const cookie = req.cookies.token;

            res.cookie("authToken", token);

            let responseData = {
              _id: user._id,
              fullName: user.fullName,
              email: user.email,
              mobileNumber: user.mobileNumber,
              jwtToken: token,
            };

            res.json({
              error: false,
              data: responseData,
              message: "Login Successfully",
            });
          } else {

            res.json({
              error: true,
              passwordErr: true,
              message: 'Incorrect password! Please check your password and try again'
            })
          }
        } else {
          res.json({
            error: true,
            blockErr: true,
            message: 'Sorry!!! You have been temporarily Blocked by Admin'
          })
        }
      } else {

        res.json({
          error: true,
          emailErr: true,
          message: 'Incorrect Email! Please check your Email and try again'
        })

      }
    } catch (err) {
      console.log(err);

      res.status(500).json({
        error: true,
        message: err + "",
      });


    }
  },

  getResetOtp: async (req, res, next) => {
    try {

      // validate incoming data
      let params = req.body

      const dataValidation = validateGetResetOtp(params)

      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, "");
        return res.json({
          error: true,
          message: message,
        });
      }


      // send OTP to email if entered value is a proper email address
      if (validateEmail(params.emailOrMobile)) {
        const userData = await User.findOne({ email: params.emailOrMobile })
        if (userData) {
          const otp = Math.floor(100000 + Math.random() * 900000)
          const name = userData.fullName
          let otpData = await resetPassword.findOneAndUpdate({ $or: [{ email: params.emailOrMobile }, { mobile: userData.mobileNumber }] }, { email: params.emailOrMobile, otp }, { upsert: true, new: true })
          const emailData = resetPasswordTemplate(name, params.emailOrMobile, otpData.otp)
          const sendEmailOTP = ses.sendEmail(emailData).promise()
          sendEmailOTP.then(data => {
            console.log('Email submitted to SES', data)
            res.json({ ok: true, msg: "OTP successfully sent to your email" })
          }).catch(err => {
            console.log('SES Email Failed', err)
            return res.json({ ok: false, msg: 'Something went wrong' })
          })

        } else {
          res.json({ ok: false, msg: "Invalid User" })
        }
      }

      // send OTP to email if entered value is a proper mobile number
      else if (validateMobileNumber(params.emailOrMobile)) {

        // check whether it is valid mobile number
        const isValidUser = await User.findOne({ mobileNumber: params.emailOrMobile })

        if (!isValidUser) {
          return res.json({ ok: false, msg: 'User not found' })
        }


        // assigning 4 digit OTP and updating it in db
        const otp = Math.floor(100000 + Math.random() * 900000)

        let otpData = await resetPassword.findOneAndUpdate({ $or: [{ mobile: params.emailOrMobile }, { email: isValidUser.email }] }, { mobile: params.emailOrMobile, otp }, { upsert: true, new: true })


        // sending SMS OTP
        await axios.post(`http://2factor.in/API/V1/${process.env.SMS_API_KEY}/SMS/${params.emailOrMobile}/${otpData.otp}/SKILL2FLY`, {
          headers: {
            "content-type": "application/x-www-form-urlencoded"
          }
        }).then((response) => {
          return res.json({ ok: true, msg: 'OTP successfully sent to your mobile' })
        }).catch((error) => {
          console.log('sms sending error: ', error)
          return res.json({ ok: false, msg: 'Something went wrong' })
        })

      }

      // return error msg for invalid value
      else {
        return res.json({ ok: false, msg: "Please enter valid email or mobile" })
      }

    } catch (err) {
      console.log(err)
      return res.json({ ok: false, msg: 'Something went wrong' })
    }
  },

  submitOtp: async (req, res, next) => {
    try {
      const { emailOrMobile, otp } = req.body

      let isMobile = validateMobileNumber(emailOrMobile)
      let isEmail = validateEmail(emailOrMobile)

      const otpData = await resetPassword.findOne({ 
        ...(isEmail && {email: emailOrMobile}), 
        ...(isMobile && {mobile: emailOrMobile}), 
        otp })
      if (otpData) {
        res.json({ ok: true, message: "OTP VERIFIED" })
      } else {
        res.json({ ok: false, msg: "INVALID OTP" })
      }
    } catch (err) {
      console.log(err)
    }
  },

  resetPassword: async (req, res, next) => {
    try {

      // validate incoming data
      let params = req.body

      const dataValidation = validateResetPassword(params)

      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, "");
        return res.json({
          error: true,
          message: message,
        });
      }


      // check whether password and confirm password are matching 
      if (params.password !== params.confirmPassword) {
        return res.json({
          ok: false,
          msg: 'Passwords did not match'
        })
      }


      // verifying otp
      let isMobile = validateMobileNumber(params.emailOrMobile)
      let isEmail = validateEmail(params.emailOrMobile)

      const otpData = await resetPassword.findOne({ 
        ...(isEmail && {email: params.emailOrMobile}), 
        ...(isMobile && {mobile: params.emailOrMobile}),
        otp: params.otp 
      })

      if (!otpData) {
        res.json({ ok: true, message: "OTP Error" })
      }


      // hash password
      let newPassword = await bcrypt.hash(params.password, 10);


      // update password
      await User.updateOne({
        ...(isMobile && {mobileNumber: params.emailOrMobile}),
        ...(isEmail && {email: params.emailOrMobile})
      }, {
        $set: {
          password: newPassword
        }
      }).then((response) => {
        res.json({ ok: true, msg: 'Password successfully updated'})
      }).catch((err) => {
        res.json({ ok: false, msg: 'Error in resetting password'})
      })


    } catch (err) {
      console.log('error:', err)
      res.json({ ok: false, msg: 'Something went wrong' })
    }
  },

  signup: async (req, res) => {
    console.log(req.body);

    let params = req.body;

    // data validation
    const dataValidation = await signupValidation(params);
    if (dataValidation.error) {
      const message = dataValidation.error.details[0].message.replace(/"/g, "");
      console.log(message);
      return res.json({
        error: true,
        data: "",
        message: message,
      });
    }

    try {
      // Existing validation
      let userExist = await User.findOne({ email: req.body.email });
      if (userExist) {
        console.log("user already exists");
        return res.json({
          error: true,
          data: "",
          message: "Email is already taken, try another email.",
        });
      }

      // password hashing
      const hashedPassword = await bcrypt.hash(req.body.password, 10);

      //register
      const user = new User({
        fullName: req.body.fullName,
        email: req.body.email,
        password: hashedPassword,
        mobileNumber: req.body.phoneNumber,
        referredBy: req.body.referralId
      });
      let savedUser = await user.save();

      // create signed jwt
      const token = await jwt.sign(
        { _id: savedUser._id },
        process.env.JWT_SECRET,
        {
          expiresIn: "30d",
        }
      );

      // send token in cookie
      const cookie = req.cookies.token;

      res.cookie("authToken", token);

      let responseData = {
        _id: savedUser._id,
        fullName: savedUser.fullName,
        email: savedUser.email,
        mobileNumber: savedUser.mobileNumber,
        jwtToken: token,
      };
      console.log(responseData, "registration Success");

      // send user as json response
      res.json({
        error: false,
        data: responseData,
        message: "Registered successfully",
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        error: true,
        message: err + "",
      });
    }
  },
  home: async (req, res) => {
    try {
      console.log("hai");
    } catch (err) { }
  },

  // for google sig

  googleSignup: async (req, res) => {

    console.log(req.body);


    try {


      let userExists = await User.findOne({ email: req.body.email });

      if (userExists) {
        return res.json({
          error: true,
          data: "",
          message: "Email is already registered. Please login",
        });

        // user prompts to login automatically 

        // this.googleLogin(req);
      }
      else {
        const user = new User({
          fullName: req.body.userName,
          email: req.body.email,
          mobileNumber: '',
          loginType: "google"
        });

        let savedUser = await user.save();

        // create signed jwt
        const token = await jwt.sign(
          { _id: savedUser._id },
          process.env.JWT_SECRET,
          {
            expiresIn: "30d",
          }
        );

        // send token in cookie
        const cookie = req.cookies.token;

        res.cookie("authToken", token);

        let responseData = {
          _id: savedUser._id,
          fullName: savedUser.fullName,
          email: savedUser.email,
          mobileNumber: "",
          jwtToken: token,
        };
        console.log(responseData, "registration Success");

        // send user as json response
        res.json({
          error: false,
          data: responseData,
          message: "Registered successfully",
        });

      }
    }
    catch (err) {

      console.log("err", err)
      res.json({
        error: err,
        message: err,
      });

    }


  },

  googleLogin: async (req, res) => {

    try {
      console.log(req.body)
      let user = await User.findOne({ email: req.body.email });
      if (user) {

        const token = await jwt.sign(
          { _id: user._id },
          process.env.JWT_SECRET,
          {
            expiresIn: "30d",
          }
        );
        // send token in cookie
        const cookie = req.cookies.token;

        res.cookie("authToken", token);

        let responseData = {
          _id: user._id,
          fullName: user.fullName,
          email: user.email,
          mobileNumber: "",
          jwtToken: token,
        };

        res.json({
          error: false,
          data: responseData,
          message: "Login Successfully",
        });


      } else {
        console.log("I dont know this guy");

        res.json({
          error: true,
          data: responseData,
          message: "User is not registered yet",
        });
      }
    } catch (err) {
      console.log(err);

      res.status(500).json({
        error: true,
        message: err + "",
      });


    }
  },
  getProfileData: async (req, res, next) => {
    try {
      let userData
      if (req.user.purchasedCourses) {
        userData = await User.findOne({ _id: req.user._id }).populate('purchasedCourses')
      } else {
        userData = req.user
      }

      let profileData = {
        profileName: userData.fullName,
        profileEmail: userData.email,
        profilePhone: userData.mobileNumber,
        profileCountry: userData.metaData?.country ?? '-',
        profileState: userData.metaData?.state ?? '-',
        profileCity: userData.metaData?.city ?? '-',
        profileQualification: userData.metaData?.qualification ?? '-',
        profileDesignation: userData.metaData?.designation ?? '-',
        profileCourse: userData.purchasedCourses ?? [],
        profileEnable: true
      }
      res.json({ error: false, profileData })


    } catch (err) {
      console.log(err);
    }
  },
  updateProfile: async (req, res, next) => {
    try {
      const dataValidation = await profileUpdateValidation(req.body);
      if (dataValidation.error) {
        const message = dataValidation.error.details[0].message.replace(/"/g, "");
        console.log(message);
        return res.json({
          error: true,
          data: "",
          message: message,
        });
      }
      const { fullName, phoneNumber, email, country, state, city, designation, qualification } = req.body

      let data = {
        fullName,
        mobileNumber: phoneNumber,
        metaData: {
          country,
          state,
          city,
          designation,
          qualification
        }
      }
      let userData = await User.findOneAndUpdate({ _id: req.user._id }, data, { upsert: true, new: true })
      let profileData = {
        profileName: userData.fullName,
        profileEmail: userData.email,
        profilePhone: userData.mobileNumber,
        profileCountry: userData.metaData?.country ?? '-',
        profileState: userData.metaData?.state ?? '-',
        profileCity: userData.metaData?.city ?? '-',
        profileQualification: userData.metaData?.qualification ?? '-',
        profileDesignation: userData.metaData?.designation ?? '-',
        profileCourse: userData.purchasedCourses ?? [],
        profileEnable: true
      }
      res.json({ error: false, message: "Profile Updation Successful", profileData })

    } catch (err) {
      console.log(err);
    }
  },

  updateProfilePic: async (req, res) => {
    try {
      let profileImg = req.files.image
      console.log(profileImg, "file data");
      let notImage = FileValidation.checkImageType(profileImg)
      if (notImage == false) {
        let s3bucket = new AWS.S3({
          accessKeyId: process.env.IAM_ACCESS_KEY,
          secretAccessKey: process.env.IAM_SECRET_KEY,
          Bucket: process.env.AWS_USER_BUCKET
        });
        s3bucket.createBucket(function () {
          var params = {
            Bucket: process.env.AWS_USER_BUCKET,
            Key: '' + req.user._id + '.jpg',
            Body: profileImg.data
          };
          s3bucket.upload(params, function (err, data) {
            if (err) {
              console.log('error in callback');
              console.log(err);
              return res.json({
                error: true,
                message: 'Image Upload Failed!!'
              })
            } else {
              console.log('<<===S3 IMG UPLOAD SUCCESS===>>');
              console.log(data);
              return res.json({
                error: false,
                message: 'Image uploaded Successfully'
              })
            }

          });
        });
      } else {
        return res.json({
          error: true,
          message: 'File is not an Image'
        })
      }
    } catch (error) {
      return res.json({
        error: true,
        message: 'Server Error!!!'
      })
    }
  }
};
