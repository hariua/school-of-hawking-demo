const Joi = require('joi')


const loginValidation=(data)=>{

    const Schema = Joi.object({

        email:Joi.string().required().email({minDomainSegments:2,tlds:{allow:['com','net']}}),
        password: Joi.string()
            .required()
            .label("Password")
            .min(6)
            .max(30)
            .messages({ "string.pattern.name": "Invalid password" }),

    })
    return Schema.validate(data)
}




const signupValidation = (data) => {
    const Schema = Joi.object({
        fullName: Joi.string()
            .required()
            .label("Fullname")
            .messages({ "string.pattern.name": "Invalid fullname." }),
        email: Joi.string().required()
            .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
        password: Joi.string()
            .required()
            .label("Password")
            .min(6)
            .max(30)
            .messages({ "string.pattern.name": "Invalid password" }),
        phoneNumber: Joi.string().min(10).required().label("phone number"),
        referralId: Joi.any()

    })

    return Schema.validate(data)

}

const profileUpdateValidation = (data) => {
    const Schema = Joi.object({
        fullName: Joi.string()
            .required()
            .label("Fullname")
            .messages({ "string.pattern.name": "Invalid fullname." }),
        email: Joi.string().required()
            .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
        country: Joi.string().required().invalid('-').label("Country"),
        state: Joi.string().required().invalid('-').label("State"),
        city: Joi.string().required().invalid('-').label("City"),
        designation: Joi.string().invalid('-').required().label("Designation"),
        qualification: Joi.string().invalid('-').required().label("Qualification"),
        phoneNumber: Joi.string().min(10).required().label("phone number")
    })

    return Schema.validate(data)
}

const validateGetResetOtp = (data) => {
    console.log('data',data)
    const Schema = Joi.object({
        emailOrMobile: Joi.string().required().label('emailOrMobile')
    })

    return Schema.validate(data)
}

const validateResetPassword = (data) => {
    const Schema = Joi.object({
        password: Joi.string().required().label('password'),
        confirmPassword: Joi.string().required().label('confirmPassword'),
        otp: Joi.string().required().label('otp'),
        emailOrMobile: Joi.string().required().label('emailOrMobile'),
    })

    return Schema.validate(data)
}

module.exports = {
    signupValidation,
    loginValidation,
    profileUpdateValidation,
    validateGetResetOtp,
    validateResetPassword,
};
