const resetPasswordTemplate = (name,email,otp) => {
    return {
        Source: process.env.EMAIL_FROM_ADDRESS,
        Destination: {
            ToAddresses: [email],
        },
        ReplyToAddresses: [process.env.EMAIL_TO_ADDRESS],
        Message: {
            Body: {
                Html: {
                    Charset: 'UTF-8',
                    Data: `
                        <html>
                            <body>
                                <h4> Dear ${name} </h4>
                                <p>This email is sending on behalf of Skill2Fly for Resetting your Password </p>
                                <p>Following is your OTP to reset your password</p>
                                <h3>${otp}</h3>
                            </body>
                        </html>       
                    `,
                },
            },
            Subject: {
                Charset: 'UTF-8',
                Data: 'Reset Password SKILL2FLY',
            },
        },
    }
}

module.exports={resetPasswordTemplate}