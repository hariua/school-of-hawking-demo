import axios from 'axios';
import React from 'react'
import { useState } from 'react';
import { useEffect } from 'react';
import Loader from "react-loader-spinner";
import { useSelector } from 'react-redux';
import './CareerDepartment.css'
import { Modal, Button, Form } from 'react-bootstrap';
import toast from "react-hot-toast"

function CareerDepartment() {
    const [departments, setDepartments] = useState([])
    const [loading, setLoading] = useState(false)
    const [bool, setBool] = useState(false)
    const [show, setShow] = useState(false);



    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    const adminDetails = useSelector(state => state.adminData)
    useEffect(() => {
        loadUserData();
    }, [bool])

    const loadUserData = async () => {
        setLoading(true)
        let { data } = await axios.get(process.env.REACT_APP_SERVER + '/admin/getCareerDepartments', {
            headers: {
                authorization: "AdminJwt " + adminDetails.adminJwt,
            },
        })
        setDepartments(data.data)
        setLoading(false)
    }

    const AddNewQuestion = async () => {

        let { data } = await axios.post(process.env.REACT_APP_SERVER + '/admin/addNewQuestion', { fieldValues }, {
            headers: {
                authorization: "AdminJwt " + adminDetails.adminJwt,
            },
        })
        console.log("last", data)
        if (data.error) {
            toast.error(data.message)
        }
        else {
            toast.success(data.message)
            loadUserData();
            setShow(false)
        }
    }

    const [fieldValues, setFieldValues] = useState({

        question: '',
        a: '',
        b: '',
        c: '',
        d: '',
        right: '',
        comments: ''

    })

    const handleChange = (name) => async (event) => {
        setFieldValues({ ...fieldValues, [name]: event.target.value })
    }

    return (
        <>

            {loading ?
                <>    <Loader
                    type="Circles"
                    color="#00BFFF"
                    height={100}
                    width={100}

                />
                </> : <>


                    {/* admin add modal */}

                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Add A New Admin</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>

                            <Form>
                                <Form.Group className="mb-3" controlId="formBasicEmail">
                                    <Form.Label>Department Name</Form.Label>
                                    <Form.Control type="text" placeholder="Enter Name" onChange={handleChange('question')} required />
                                </Form.Group>
                            </Form>

                            <Button variant="primary" type="submit" onClick={AddNewQuestion}>
                                Submit
                            </Button>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>

                        </Modal.Footer>
                    </Modal>

                    <div className='departments-header'>
                        <h3>CAREER DEPARTMENTS</h3>
                    </div>


                    <table class="table" id="userTable">

                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Department Name</th>
                                <th scope="col"></th>

                            </tr>
                        </thead>
                        <tbody>
                            {departments.map((data, i) => {

                                return (

                                    <>
                                        <tr>
                                            <th scope="row">{i + 1}</th>
                                            <td>{data.departmentName}</td>
                                            <td><Button variant="primary" onClick={handleShow} className="float-right">
                                                Edit
                                            </Button></td>
                                            {/* <td>{data.loginType}</td>
                      <td>{data.isBlocked ? <>Blocked</> : <>Not Blocked</>}</td>
                      {data.isBlocked ? <>
                        <td><label class="switch">
                          <input type="checkbox" checked onChange={(e) => { unBlockUser(data._id) }} />
                          <span class="slider round"></span>
                        </label></td>
                      </> : <>
                        <td><label class="switch">
                          <input type="checkbox" onChange={(e) => { BlockUser(data._id) }} />
                          <span class="slider round"></span>
                        </label></td>
                      </>}
                      <td><button className="btn btn-danger" onClick={(e)=>{dismissAdmin(data._id)}}> Dismiss</button></td> */}
                                        </tr>
                                    </>
                                )

                            })}

                        </tbody>
                    </table>





                </>}



        </>
    )
}

export default CareerDepartment
