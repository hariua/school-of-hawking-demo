import axios from "axios";
import React from "react";
import { useEffect, useState } from "react";
import Footer from "../Footer/Footer";
import Navbar from "../NavBar/Navbar";
import { useDispatch, useSelector } from "react-redux";
import toast from "react-hot-toast";
import { useHistory } from "react-router";
import "./PaymentGateway.css";
import { profileData as profileAction } from "../../../redux/rootActions";
import Loader from "react-loader-spinner";
import Swal from "sweetalert2";
function loadScript(src) {
  return new Promise((resolve) => {
    const script = document.createElement("script");
    script.src = src;
    script.onload = () => {
      resolve(true);
    };
    script.onerror = () => {
      resolve(false);
    };
    document.body.appendChild(script);
  });
}

function PaymentGateway() {
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.userData);
  const courseData = useSelector((state) => state.purchaseCourseData);
  const history = useHistory();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (courseData.actualPrice != "") {
      loadScript("https://checkout.razorpay.com/v1/checkout.js");
    } else {
      console.log("IIII", courseData);
      history.push("/course");
    }
  }, [courseData]);

  async function displayRazorpay() {
    setLoading(true);

    let { data } = await axios.post(
      process.env.REACT_APP_SERVER + "/createOrder",
      { id: userData.userId, courseData },
      {
        headers: {
          authorization: "Bearer " + userData.userJwt,
        },
      }
    );
    if (data.error) {
      toast.error("something went wrong");
      console.log("error", data);
      setLoading(false);
    } else {
      console.log("__", data);
      var options = {
        key: process.env.RAZORPAY_KEY, // Enter the Key ID generated from the Dashboard
        amount: data.data.amount, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
        currency: "INR",
        name: "SKILL2FLY",
        description: "school-of-hawking LLP",
        image: "https://example.com/your_logo",
        order_id: data.data.id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
        handler: function (response) {
          setLoading(true);
          console.log("resppp", response);
          // alert(response.razorpay_payment_id);
          // alert(response.razorpay_order_id);
          // alert(response.razorpay_signature);
          //to backend update the course purchase
          PaymentSuccess(response);
        },
        prefill: {
          name: userData.userName,
          email: userData.userMail,
          contact: userData.userPhone,
        },
        notes: {
          address: "Razorpay Corporate Office",
        },
        theme: {
          color: "#3399cc",
        },
      };
      const paymentObject = new window.Razorpay(options);
      paymentObject.open();
      setLoading(false);
    }
  }

  const PaymentSuccess = async (razorpayResponse) => {
    setLoading(true);
    try {
      let { data } = await axios.post(
        process.env.REACT_APP_SERVER + "/purchaseSuccess",
        { razorpayResponse, courseData, userData },
        {
          headers: {
            authorization: "Bearer " + userData.userJwt,
          },
        }
      );
      console.log("data", data);
      if (data.error) {
        setLoading(false);
        toast.error(
          "something went wrong in payment please contact our team..."
        );
      } else {
        getProfileData();
        toast.success(
          "Payment successfully completed! check Your profile to view courses "
        );

        if (data.data.is360) {
          history.push("/aptitude-test");
        } else {
          history.push("/profile");
        }
      }
    } catch (err) {
      setLoading(false);
      toast.error("something went wrong in payment please contact our team...");
    }
  };
  const [contactDone, setContactDone] = useState(false);

  const contactAdmin = async () => {
    let userId = userData.userId;

    try {
      let { data } = await axios.post(
        process.env.REACT_APP_SERVER + "/getHelp",
        { userId },
        {
          headers: {
            authorization: "Bearer " + userData.userJwt,
          },
        }
      );

      console.log("!!!!!!!", data);
      if (data.error == false) {
        toast.success("success you will get a call soon");

        document.getElementById("contactbtn").style = "disply:none";
        document.getElementById("sucesscallbtn").style = "disply:block";
        setContactDone(true);
      } else {
        // toast.error("something went wrong");
      }
    } catch (err) {
      // toast.error("something went wrong");
    }
  };

  const getProfileData = () => {
    setLoading(true);
    axios
      .get(process.env.REACT_APP_SERVER + "/getProfileData", {
        headers: {
          authorization: "Bearer " + userData.userJwt,
        },
      })
      .then((response) => {
        if (response.data.error === false) {
          dispatch(profileAction(response.data.profileData));
          let data = response.data.profileData;
        } else {
          toast.error(response.data.message);
        }
        setLoading(false);
      });
  };

  function enableSwal() {
    let title = "You have'nt logged In yet";
    Swal.fire({
      icon: "warning",
      iconColor: "#8536f5",
      showCloseButton: true,
      title: "<h5 style='color:#8536f5'>" + title + "</h5>",
      showCancelButton: true,
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonColor: "#8536f5",
      confirmButtonText: "Go To Login",
      denyButtonText: "Register Now",
      denyButtonColor: "#8536f5",
      background: "#ffffff",
    }).then((result) => {
      if (result.isConfirmed) {
        history.push("/login");
      } else if (result.isDenied) {
        history.push("/signup");
      } else {
        // enableSwal();
      }
    });
  }

  return (
    <div>
      <Navbar />

      <section
        className="page-banner"
        style={{ backgroundImage: "url(assets/skill2fly/banner2.png)" }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h4 className="banner-title">Proceed to Payment</h4>
              <div className="bread-crumbs">
                {/* <a href="index.html">Home</a> <span /> Payment gateway */}
              </div>
            </div>
          </div>
        </div>
      </section>

      {loading ? (
        <>
          <Loader type="ThreeDots" color="#5838fc" height={100} width={100} />
          <p style={{ color: "black" }}>Don't go back or close the window</p>
        </>
      ) : (
        <>
          <section className="contact-section">
            <div className="container">
              <div className="row">
                <div className="col-md-4">
                  <div className="contact--info-area">
                    <h3 style={{ lineHeight: "1.2" }}>
                      Enroll the course For ₹{courseData.discountPrice}
                    </h3>
                    <div className="hideMob">
                      <p>
                        Any issues with purchase..? feel free to contact us for
                        purchase related queries.
                      </p>
                      <div className="single-info" style={{display:"none"}}>
                        <h5>Headquaters</h5>
                        <p>
                          <i className="icon_house_alt" />
                          Hilite Business Park, 5th floor (codelattice) Craft
                          Square
                          <br /> Kozhikode, Kerala 673014
                        </p>
                      </div>

                      <div className="single-info">
                        <h5>Support</h5>
                        <p>
                          <i className="icon_mail_alt" />
                          <a href="mailto:schoolofhawking@gmail.com">
                            schoolofhawking@gmail.com
                          </a>{" "}
                          <br />
                        </p>
                      </div>
                      <div className="ab-social">
                        <h5>Follow Us</h5>
                        <a className="fac" href="#">
                          <i className="social_facebook" />
                        </a>
                        <a className="twi" href="#">
                          <i className="social_twitter" />
                        </a>
                        <a className="you" href="#">
                          <i className="social_youtube" />
                        </a>
                        <a className="lin" href="#">
                          <i className="social_linkedin" />
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-8 paymentdiv">
                  <div className="contact-form">
                    <h4>Pay Now</h4>
                    <p>You can use any payment methods! </p>

                    <div className="col-md-6" style={{ alignItems: "center" }}>
                      {userData.userLogin ? (
                        <button
                          className="btn btn-primary btn-lg  payButton bisylms-btn"
                          onClick={displayRazorpay}
                        >
                          Pay ₹{courseData.discountPrice}
                        </button>
                      ) : (
                        <button
                          className="btn btn-primary btn-lg  payButton bisylms-btn"
                          onClick={() => {
                            enableSwal();
                          }}
                        >
                          Buy Now
                        </button>
                      )}
                    </div>
                  </div>
                  <h4 className="orline">
                    <span>(OR)</span>
                  </h4>
                  <div className="contact-form">
                    <h4>Contact our team to Buy course</h4>
                    <p>
                      click the below button our team will guide you to get the
                      course
                      <br />
                    </p>
                    {/* <div className="col-md-6" style={{ alignItems: "center" }}>
                      <input
                        type="text"
                        name="phone"
                        placeholder="Phone Number"
                        id="myPhoneNumber"
                      />
                    </div> */}
                    <div className="col-md-6" style={{ alignItems: "center" }}>
                      {!contactDone ? (
                        <>
                          <button
                            className="btn btn-primary btn-lg  payButton bisylms-btn"
                            onClick={contactAdmin}
                            id="contactbtn"
                          >
                            Request for help to buy course
                          </button>
                        </>
                      ) : (
                        <button
                          className="btn btn-primary btn-lg  payButton"
                          id="sucesscallbtn"
                          style={{ display: "none" }}
                        >
                          our team will connect you soon!
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </>
      )}

      <Footer />
    </div>
  );
}

export default PaymentGateway;
