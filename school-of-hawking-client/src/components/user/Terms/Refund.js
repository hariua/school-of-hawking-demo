import React from 'react';
import Footer from '../Footer/Footer';
import Navbar from '../NavBar/Navbar';

function Refund() {
  return <div>
  <div>
    <Navbar />
    <section
      className="page-banner"
      style={{ backgroundImage: "url(assets/skill2fly/banner2.png)" }}
    >
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <h2 className="banner-title">Refund  Policy</h2>
            <div className="bread-crumbs">
              <a href="index.html">Home</a> <span /> Refund Policy
            </div>
          </div>
        </div>
      </div>
    </section>

    <section className="contact-section">
      <div className="container">
        <div className="row">
          <div className="contact-form">
            <h4>Refund Policy</h4>
            {/* <p>Integer at lorem eget diam facilisis lacinia ac id massa.</p> */}
            <div className="container">
            <h3>HOW TO REQUEST FOR REFUND/CANCELLATION</h3>
              <p>Last updated: January 22, 2022</p>
              <p style={{textAlign:"justify"}}>
              We have a 1-day return policy, which means you have 1 day after purchasing your course.<br/>
             please read the below guidelines to get your refund
            <ul>
              <li> Refund requests cannot be submitted in the app. You can mail to :<a href="mailto:schoolofhawking@gmail.com">schoolofhawking@gmail.com </a>directly us regarding the refund </li>
              <li>Refunding may take upto 5-7 business days.  </li>
              <li>It will be better to specify the reason behind cancellation of the purchase  </li>
              <li>You are not eligible for refund if you copy or spread the video you have purchased  </li>
             </ul> 
</p>
             <h3>HOW TO REQUEST FOR REFUND/CANCELLATION</h3>
             <p style={{textAlign:"justify"}}>        
          You have to mail us at :<a href="mailto:schoolofhawking@gmail.com">schoolofhawking@gmail.com </a> regarding the refund and you should specify the course name you have purchased our team will get back to you regarding the cancellation.
          
          
          
          <br/>   <br/>   Most refunds are returned via the original payment method.

                If you have any questions about this Refund and cancellation Policy, You can
                contact us:  
               By email:<a href="mailto:schoolofhawking@gmail.com">schoolofhawking@gmail.com </a>
              </p>
           
           
            </div>
          </div>
        </div>
      </div>
    </section>

    <Footer />
  </div>
</div>
}

export default Refund;
