import React from "react";
import Footer from "../Footer/Footer";
import Navbar from "../NavBar/Navbar";
import "./common.css"
function Terms() {
  return (
    <div>
      <div>
        <Navbar />
        <section
          className="page-banner"
          style={{ backgroundImage: "url(assets/skill2fly/banner2.png)" }}
        >
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <h2 className="banner-title">Terms & Conditions </h2>
                <div className="bread-crumbs">
                  <a href="index.html">Home</a> <span /> Terms & Conditions
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="contact-section">
          <div className="container">
            <div className="row">
              <div className="contact-form">
                <h4>Terms & Condition of use</h4>
                {/* <p>Integer at lorem eget diam facilisis lacinia ac id massa.</p> */}
                <div className="container">
                  <p style={{ textAlign: "justify" }}>
                    Welcome to skill2fly! These terms and conditions outline the
                    rules and regulations for the use of school of hawking's
                    Website, located at www.skill2fly.com. By accessing this
                    website we assume you accept these terms and conditions. Do
                    not continue to use skill2fly if you do not agree to take
                    all of the terms and conditions stated on this page. The
                    following terminology applies to these Terms and Conditions,
                    Privacy Statement and Disclaimer Notice and all Agreements:
                    "Client", "You" and "Your" refers to you, the person log on
                    this website and compliant to the Company’s terms and
                    conditions. "The Company", "Ourselves", "We", "Our" and
                    "Us", refers to our Company. "Party", "Parties", or "Us",
                    refers to both the Client and ourselves. All terms refer to
                    the offer, acceptance and consideration of payment necessary
                    to undertake the process of our assistance to the Client in
                    the most appropriate manner for the express purpose of
                    meeting the Client’s needs in respect of provision of the
                    Company’s stated services, in accordance with and subject
                    to, prevailing law of Netherlands. Any use of the above
                    terminology or other words in the singular, plural,
                    capitalization and/or he/she or they, are taken as
                    interchangeable and therefore as referring to same.
                  </p>

                  <br />
                  <br />
                  <h3>Cookies</h3>
                  <p style={{ textAlign: "justify" }}>
                    We employ the use of cookies. By accessing skill2fly, you
                    agreed to use cookies in agreement with the school of
                    hawking's Privacy Policy. Most interactive websites use
                    cookies to let us retrieve the user’s details for each
                    visit. Cookies are used by our website to enable the
                    functionality of certain areas to make it easier for people
                    visiting our website. Some of our affiliate/advertising
                    partners may also use cookies.
                  </p>
                </div>



                <div className="container" id="privacy-policy">
                  <p style={{ textAlign: "justify" }}>
                    Welcome to skill2fly! These terms and conditions outline the
                    rules and regulations for the use of school of hawking's
                    Website, located at www.skill2fly.com. By accessing this
                    website we assume you accept these terms and conditions. Do
                    not continue to use skill2fly if you do not agree to take
                    all of the terms and conditions stated on this page. The
                    following terminology applies to these Terms and Conditions,
                    Privacy Statement and Disclaimer Notice and all Agreements:
                    "Client", "You" and "Your" refers to you, the person log on
                    this website and compliant to the Company’s terms and
                    conditions. "The Company", "Ourselves", "We", "Our" and
                    "Us", refers to our Company. "Party", "Parties", or "Us",
                    refers to both the Client and ourselves. All terms refer to
                    the offer, acceptance and consideration of payment necessary
                    to undertake the process of our assistance to the Client in
                    the most appropriate manner for the express purpose of
                    meeting the Client’s needs in respect of provision of the
                    Company’s stated services, in accordance with and subject
                    to, prevailing law of Netherlands. Any use of the above
                    terminology or other words in the singular, plural,
                    capitalization and/or he/she or they, are taken as
                    interchangeable and therefore as referring to same.
                  </p>

                  <br />
                  <br />
                  <h3>Cookies</h3>
                  <p style={{ textAlign: "justify" }}>
                    We employ the use of cookies. By accessing skill2fly, you
                    agreed to use cookies in agreement with the school of
                    hawking's Privacy Policy. Most interactive websites use
                    cookies to let us retrieve the user’s details for each
                    visit. Cookies are used by our website to enable the
                    functionality of certain areas to make it easier for people
                    visiting our website. Some of our affiliate/advertising
                    partners may also use cookies.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <Footer />
      </div>
    </div>
  );
}

export default Terms;
