import React from "react";
import { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Error from "../Error";
import Contact from "./Contact/Contact";
import CourseList from "./CourseList/CourseList";
import Index from "./Index/Index";
import PaymentGateway from "./PaymentGateway/PaymentGateway";
import Profile from "./Profile/Profile";
import Questions from "./Questions/Questions";
import Signup from "./Signup/Signup";
import SingleCourse from "./SingleCourse/SingleCourse";
import Privacy from "./Terms/Privacy";
import Refund from "./Terms/Refund";
import Terms from "./Terms/Terms";

function User() {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, [window.location]);

  return (
    <div>
      <Switch>
        <Route path="/" exact>
          <Index />{" "}
        </Route>
        <Route path="/signup">
          <Signup login={false} />
        </Route>
        <Route path="/login">
          <Signup login={true} />
        </Route>
        <Route path="/course">
          <CourseList />
        </Route>
        <Route path="/contact">
          <Contact />
        </Route>
        <Route path="/profile">
          <Profile />
        </Route>
        <Route path="/singlecourse/:id">
          <SingleCourse />
        </Route>
        <Route path="/aptitude-test">
          <Questions />
        </Route>
        <Route exact path="/make-payment">
          <PaymentGateway />
        </Route>
        <Route exact path="/terms-and-conditions">
          <Terms/>
        </Route>
        <Route exact path="/privacy-policy">
          <Privacy/>
        </Route>
        <Route exact path="/refund-policy">
          <Refund/>
        </Route>
      </Switch>
    </div>
  );
}

export default User;
