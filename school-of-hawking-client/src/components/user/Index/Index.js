import React from "react";
import Footer from "../Footer/Footer";
import Navbar from "../NavBar/Navbar";
import BannerOne from "./Banners/BannerOne";
import BannerTwo from "./Banners/BannerTwo";
import BannerThree from './Banners/BannerThree';
import CourseCard from "./Banners/CourseCard";
import HomeVideo from "./Banners/HomeVideo";
import UpcomingEvents from "./Banners/UpcomingEvents";
import Memberships from "./Banners/Memberships";
import Blogs from "./Banners/Blogs";
import { Link } from "react-router-dom";


function Index() {
  return (
    <>
   
      <Navbar />
      <section
        className="cta-section-2"
        style={{ backgroundImage: "url(assets/images/home3/4.jpg)" }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-8 offset-lg-2 text-center">
              <h2 className="sec-title mb-15">
               SKILL2FLY
                <br /> Hybrid Learning Experience
              </h2>
              <p>
              The Future Belongs To Those Enthusiastic, Who Learn And Adapt New Skills And Change Their Future Forever
                <br /> 
              </p>
              <a href="#" className="bisylms-btn">
             <Link to="/course" style={{color:"white"}}> Explore Programs</Link>  
              </a>
            </div>
          </div>
        </div>
      </section>
      {/* Call To Action End */}
      {/* <BannerOne/> */}
      <BannerTwo/>
      <BannerThree/>
      {/* <CourseCard/> */}
      {/* <HomeVideo/>
      <UpcomingEvents/>
      <Memberships/> 
      <Blogs/>  */}

      <Footer/>
    </>
  );
}

export default Index;
