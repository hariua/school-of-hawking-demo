import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { courseAction } from "../../../../redux/rootActions";
import Footer from "../../Footer/Footer";
import Navbar from "../../NavBar/Navbar";
import Loader from "react-loader-spinner";
import { useState } from "react";
export default function CourseCard() {
  const courseDetails = useSelector((state) => state.courseData);
  const userData = useSelector((state) => state.userData);
  const dispatch = useDispatch();
  const history = useHistory();
  const [dataCourse, setData] = useState([0, 1]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    if (courseDetails.course.length < 1) {
      loadCourseData();
    }
    // else {
    //   setData(courseDetails.course)
    // }
  }, []);
  const loadCourseData = async () => {
    setLoading(true);
    axios
      .get(process.env.REACT_APP_SERVER + "/getCourses", {
        headers: {
          authorization: "Bearer " + userData.userJwt,
        },
      })
      .then((response) => {
        if (response.data.error == false) {
          let courseData = response.data.data.reverse();
          //setCourse(courseData)
          let data = {
            courseData,
          };
          dispatch(courseAction(data));
          setLoading(false);
        } else {
          alert("e");
          console.log("err", response);
        }
      });
  };
  // useEffect(() => {
  //   if (courseDetails?.course) {
  //     let dataarry=courseDetails?.course
  //     console.log("dataaryy",dataarry);
  //     setData(dataarry);

  //   }
  // }, [courseDetails]);

  // useEffect(() => {

  //   if (dataCourse) {
  //     console.log(dataCourse);
  //   }
  // }, [dataCourse]);
  const courseClick = (id) => {
    history.push("/singlecourse/" + id);
  };

  const queryParams = new URLSearchParams(window.location.search);

  useEffect(() => {
    let referredBy = queryParams.get("referral");
    if (referredBy) {
      localStorage.setItem("referralId", referredBy);
    } else {
      referredBy = localStorage.getItem("referralId");
    }
  }, []);

  return (
    <div>
      <section className="feature-course-section">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <h2 className="sec-title">Pick and Learn the right course</h2>
            </div>
            <div className="col-md-7">
              {/* <ul className="shaf-filter">
                <li className="active" data-group="all">All</li>
                <li data-group="development">Web Development</li>
                <li data-group="architecture">Architecture</li>
                <li data-group="engineering">Engineering</li>
                <li data-group="science">Data Science</li>
              </ul> */}
            </div>
          </div>
          <div className="row shafull-container">
            {courseDetails.course.length > 0
              ? courseDetails.course.map((data, i) => {
                  if (i < 2) {
                    return (
                      <div className="col-lg-4 col-md-6 shaf-item">
                        <div
                          className="feature-course-item"
                          style={{ cursor: "pointer" }}
                          onClick={() => courseClick(data._id)}
                        >
                          <div className="flipper">
                            <div className="front">
                              <div className="fcf-thumb">
                                <img
                                  src={
                                    process.env.REACT_APP_S3_COURSE_BUCKET +
                                    data._id +
                                    ".jpg"
                                  }
                                  onError={(e) => {
                                    e.target.onerror = null;
                                    e.target.src =
                                      "assets/images/home/course/2.png";
                                  }}
                                  alt=""
                                />
                              </div>
                              <p
                                target="_blank"
                                href={
                                  "https://www.google.com/search?q=" +
                                  data.courseCategory.categoryName
                                }
                              >
                                {data.courseCategory.categoryName}
                              </p>
                              <h4>{data.courseName}</h4>
                              <div
                                className="fcf-bottom"
                                style={{ justifyContent: "space-evenly" }}
                              >
                                <a>
                                  <i className="icon_book_alt" />
                                  {data.subCourses.length + " Sections"}
                                </a>
                                <a>
                                  <i className="far fa-clock" />
                                  {data.duration}
                                </a>
                              </div>
                            </div>
                            <div className="back">
                              <div className="fcf-thumb">
                                <img
                                  src={
                                    process.env.REACT_APP_S3_COURSE_BUCKET +
                                    data._id +
                                    ".jpg"
                                  }
                                  onError={(e) => {
                                    e.target.onerror = null;
                                    e.target.src =
                                      "assets/images/home/course/1.png";
                                  }}
                                  alt=""
                                />
                              </div>

                              <h4>{data.courseName}</h4>
                              {/* <div className="ratings">
                                            <i className="icon_star" />
                                            <i className="icon_star" />
                                            <i className="icon_star" />
                                            <i className="icon_star" />
                                            <i className="icon_star" />
                                            <span>4.5 (1 Reviews)</span>
                                        </div> */}
                              <div className="course-price">
                                {"₹" + data.discountPrice + ".00"}
                                <div
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    justifyContent: "center",
                                  }}
                                >
                                  <span>{"₹" + data.actualPrice + ".00"}</span>
                                  <a
                                    style={{
                                      fontSize: "medium",
                                      margin: "5px 10px",
                                    }}
                                  >
                                    {data.discountPercentage + "% OFF"}
                                  </a>
                                </div>
                              </div>
                              <a
                                target="_blank"
                                href={
                                  "https://www.google.com/search?q=" +
                                  data.courseCategory.categoryName
                                }
                                className="c-cate"
                              >
                                {data.courseCategory.categoryName}
                              </a>
                              <div
                                className="author"
                                style={{ display: "block" }}
                              >
                                <img
                                  src="assets/images/home/course/author.png"
                                  alt=""
                                />
                                <a>{data.author}</a>
                              </div>
                              <div className="fcf-bottom">
                                <a>
                                  <i className="icon_book_alt" />
                                  {data.subCourses.length + " Sections"}
                                </a>
                                <a>
                                  <i className="far fa-clock" />
                                  {data.duration}
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  }
                })
              : "No course"}
            
          </div>
        </div>
      </section>
    </div>
  );
}
