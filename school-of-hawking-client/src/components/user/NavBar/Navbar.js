import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { profileData, userData } from "../../../redux/rootActions";

function Navbar() {
  let history = useHistory();
  const dispatch = useDispatch();
  const userDetails = useSelector((state) => state.userData);
  const profileDetails = useSelector((state) => state.profileData);
  useEffect(() => {}, []);
  const logout = () => {
    let title="Are You Sure to Logout?"
    Swal.fire({
      icon: "question",
      iconColor: '#8536f5',
      showCloseButton: true,
      showDenyButton: true,
      denyButtonText: 'Cancel',
      title: "<h5 style='color:#8536f5'>" + title + "</h5>",
      showConfirmButton: true,
      confirmButtonText: "Proceed to Logout",
      confirmButtonColor: "#8536f5",
      denyButtonColor: '#8536f5',
      background:"#ffffff"
    }).then((result) => {
      if (result.isConfirmed) {
        let userInfo = {
          userId: "",
          userName: "",
          userMail: "",
          userJwt: "",
          userPhone: "",
          userLogin: false,
        };
        dispatch(userData(userInfo));
        localStorage.removeItem("persist:root");
        history.push("/");
      }
    });
  };

  const [divStyle, setDivStyle] = useState({ display: "none" });

  const [collapseOn, setCollapseOn] = useState(false);

  const showMenu = () => {
    setCollapseOn(!collapseOn);
  };

  useEffect(() => {
    if (collapseOn) {
      setDivStyle({ display: "block" });
    } else {
      setDivStyle({ display: "none" });
    }
  }, [collapseOn]);
  return (
    <header className="header-01 sticky" id="navbarSch" >
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <nav className="navbar navbar-expand-lg">
              {/* logo Start*/}
              <Link className="navbar-brand " to="/">
                <img
                  src="/assets/skill2fly/logo/logo1.png"
                  className="whiteLogo"
                  alt=""
                />

                <img
                  className="sticky-logo"
                  src="/assets/skill2fly/logo/logo1.png"
                  alt=""
                />
                {/* Dont change the id of of logo...Used in script for color change!!! */}
                <span className="logoName" id="navTopLogo1" style={{fontWeight:"400px",color:"white"}}>Skill2Fly</span>
              </Link>

              <button
                className="navbar-toggler"
                onClick={() => showMenu()}
                type="button"
              >
                <i className="fal fa-bars" />
              </button>
              {/* Moblie Btn End */}
              {/* Nav Menu Start */}
              <div className="collapse navbar-collapse" style={divStyle}>
                <ul className="navbar-nav">
                  <li className="menu-item">
                    <Link to="/" style={{ textDecoration: "none" }}>
                      Home
                    </Link>
                  </li>
                  {!userDetails.userLogin && (
                    <>
                  
                      <li className="menu-item hideDesktop">
                        <Link to="/login" style={{ textDecoration: "none" }}>
                          Login
                        </Link>
                      </li>
                      <li className="menu-item hideDesktop">
                        <Link to="/signup" style={{ textDecoration: "none" }}>
                          Signup
                        </Link>
                      </li>
                      
                    </>
                  )}
                  <li className="menu-item">
                    <Link to="/course" style={{ textDecoration: "none" }}>
                      All Courses
                    </Link>
                  </li>
                  <li className="menu-item">
                    <Link to="/profile" style={{ textDecoration: "none" }}>
                      My Profile
                    </Link>
                  </li>
                  {/* <li className="menu-item">
                    <Link to="/course" style={{textDecoration:"none"}}>My Courses</Link>
                  </li> */}
                  {/* <li className="menu-item-has-children">
                      <a href="javascript:void(0);">Pages</a>
                      <ul className="sub-menu">
                        <li className="menu-item-has-children">
                          <a href="javascript:void(0);">About Pages</a>
                          <ul className="sub-menu">
                            <li><a href="about-1.html">About 01</a></li>
                            <li><a href="about-2.html">About 02</a></li>
                          </ul>
                        </li>
                        <li><a href="instructor.html">Instructor Page</a></li>
                        <li><a href="profile.html">Instructor Profile</a></li>
                        <li><a href="404.html">404 Page</a></li>
                      </ul>
                    </li>
                    <li className="menu-item-has-children">
                      <a href="javascript:void(0);">Blog</a>
                      <ul className="sub-menu">
                        <li><a href="blog.html">Blog Page</a></li>
                        <li><a href="single-post.html">Blog Details</a></li>
                      </ul>
                    </li> */}
                  <li>
                    <Link to="/contact" style={{ textDecoration: "none" }}>
                      Contact Us
                    </Link>
                  </li>
                </ul>
              </div>
              {/* Nav Menu End */}
              {/* User Btn */}
              {userDetails.userLogin == true ? (
                <div id="Logout" style={{ cursor: "pointer" }} onClick={logout}>
                  <span className="text-white" style={{ fontWeight: "600" }}>
                    Logout
                  </span>
                </div>
              ) : (
                <div id="ProfileBtn">
                  {/* <Link to="/profile">
                    <i
                      className="ti-user"
                      style={{ fontSize: "1.5em", color: "white" }}
                    />
                  </Link> */}
                </div>
              )}
              {/* User Btn */}
              {/* Join Btn */}
              {userDetails.userLogin == true ? (
                <div id="UserBtn">
                  <Link
                    to="/profile"
                    style={{ textDecoration: "none" }}
                    className="join-btn"
                  >
                    {profileDetails.profileEnable == true
                      ? profileDetails.profileName
                      : userDetails.userName}
                  </Link>
                </div>
              ) : (
                <div id="JoinBtn">
                  <Link
                    to="/signup"
                    style={{ textDecoration: "none" }}
                    className="join-btn"
                  >
                    Join for Free
                  </Link>
                </div>
              )}
              {/* This feature is under maintainace...used for putting logout btn on profile hover*/}
              {/* <ul id="userProfile" className="navbar-nav">
                <li className="menu-item-has-children">
                  <a href="javascript:void(0);"><Link to='/profile' className="join-btn">{userData.userName}</Link></a>
                  <ul className="sub-menu" style={{marginTop:"-1em"}}>
                    <li><Link to="/logout">Logout</Link></li>
                  </ul>
                </li>
              </ul> */}
            </nav>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Navbar;
