import React, { useEffect, useState } from "react";
import axios from "axios";
import Loader from "react-loader-spinner";
import { useSelector } from "react-redux";
import toast from "react-hot-toast";
import Navbar from "../NavBar/Navbar";
import Footer from "../Footer/Footer";
import { useHistory } from "react-router-dom";
import "./Questions.css";

function Questions() {
  const userData = useSelector((state) => state.userData);
  const [questions, setquestions] = useState([]);
  const [questionNumber, setQuestionNumber] = useState(1);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [totalQuestions, setTotalQns] = useState(0);
  const [answers, setAnswers] = useState([])
  const [checkedA, setCheckedA] = useState(false);
  const [checkedB, setCheckedB] = useState(false);
  const [checkedC, setCheckedC] = useState(false);
  const [checkedD, setCheckedD] = useState(false);

  const history = useHistory();

  useEffect(() => {
    axios
      .get(process.env.REACT_APP_SERVER + "/getQuestions", {
        headers: {
          authorization: "Bearer " + userData.userJwt,
        },
      })
      .then((data) => {
        if (data.data.error) {
          toast.error("something went wrong please try again later");
        } else {
          setquestions(data.data.data);
          setTotalQns(data.data.data.length);
          setCurrentQuestion(0);
        }
      });
  }, []);

  // option selecting button action
  const handleChange = async (selectedOption) => {
    if(selectedOption=="A")
    {setCheckedD(false)
      setCheckedC(false)
      setCheckedB(false)
      setCheckedA(true)
    }
    else if(selectedOption=="B")
    {setCheckedD(false)
      setCheckedC(false)
      setCheckedA(false)
      setCheckedB(true)
    }
    else if (selectedOption=="C")
    {setCheckedD(false)
      setCheckedC(false)
      setCheckedA(false)
      setCheckedC(true)
    }
    else if(selectedOption=="D")
    {setCheckedD(false)
      setCheckedC(false)
      setCheckedB(false)
      setCheckedD(true)
    }
    const search = obj => obj.question_id == questions[currentQuestion]._id;
    const existingAnswer = answers.findIndex(search)

    if (existingAnswer >= 0) {
      answers[existingAnswer].answer = selectedOption;
    } else {
      setAnswers([...answers, { "question_id": questions[currentQuestion]._id, "answer": selectedOption }]);
    }

  }

  // next button action
  const handleNextButtonClick = (answerOption) => {
      let answered = answers.find(o => o.question_id == questions[currentQuestion]._id);

      if (!answered) {
          toast.error("Please select an option");
          return;
      }

      if (currentQuestion < totalQuestions - 1) {
        const nextQuestion = currentQuestion + 1;
        setCurrentQuestion(nextQuestion);
      }

      if (currentQuestion === totalQuestions - 2) {
        document.getElementById("questionNextButton").style.display = "none";
        document.getElementById("questionSubmitButton").style.display = "block";
      }
      setCheckedD(false)
      setCheckedC(false)
      setCheckedB(false)
      setCheckedD(false)
     
  };

  // previous button action
  const handlePreviousButtonClick = () => {
    console.log('previous', answers);
    if (currentQuestion > 0) {
      const previousQuestion = currentQuestion - 1;
      setCurrentQuestion(previousQuestion);
      document.getElementById("questionSubmitButton").style.display = "none";
      document.getElementById("questionNextButton").style.display = "block";
    } else {
      alert("start of qns");
    }
  
    
      document.getElementsByClassName('spanClass').style.background='yellow'
  };

  // submit button action
  const handleSubmitButtonClick = () => {
    console.log('answers', answers);
    let answered = answers.find(o => o.question_id == questions[currentQuestion]._id);

    if (!answered) {
        toast.error("Please select an option");
        return;
    }
    
    const data = {
      answers: answers,
    };

    axios
      .post(process.env.REACT_APP_SERVER + "/submitTest", data, {
        headers: {
          authorization: "Bearer " + userData.userJwt,
        },
      })
      .then((response) => {
        console.log(data);
        if (response.data.error) {
          toast.error("something went wrong please try again later");
        } else {
          toast.success(response.data.data.message);
          setAnswers([]);
          history.push(`/singlecourse/${response.data.data.course360Id}`)
          // setQuestionNumber(1);
          // setCurrentQuestion(0);
        }
      });
  };

  const skipTest = () => {
    axios
      .post(process.env.REACT_APP_SERVER + "/skipTest",{}, {
        headers: {
          authorization: "Bearer " + userData.userJwt,
        },
      })
      .then((response) => {
        if (response.data.error) {
          toast.error("something went wrong please try again later");
        } else {
          history.push(`/singlecourse/${response.data.data.course360Id}`);
        }
      });
  }

  return (
    <div>
      <Navbar />

      <section
        className="page-banner"
        style={{ backgroundImage: "url(assets/images/banner.jpg)" }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h2 className="banner-title">Profile</h2>
              <div className="bread-crumbs">
                <a href="index.html">Home</a> <span /> Profile
              </div>
            </div>
          </div>
        </div>
      </section>


      <div className="container mt-5">
        <div className="d-flex justify-content-center row">
          <div className="col-md-10 col-lg-10">
            <div className="border">
              <div className="question bg-white p-3 border-bottom">
                <div className="d-flex flex-row justify-content-between align-items-center mcq">
                  <h4>MCQ Quiz</h4>
                  <span>({currentQuestion + 1} of {totalQuestions})</span>
                
                <button onClick={() => skipTest()} className="btn" style={{backgroundColor: "#5fa5b0", borderRadius: "15px", color: "white"}}>
                  SKIP
                </button>
                </div>
              </div>

              {questions.length ? (
                <>

                  <div className="question bg-white p-3 border-bottom">
                    <div className="d-flex flex-row align-items-center question-title">
                      <h3 className="text-danger">Q.</h3>
                      <h5 className="mt-1 ml-2">
                        {questions[currentQuestion].question}
                      </h5>
                    </div>

                    <div className="d-flex flex-row align-items-center">
                      <div className="ans ml-2" >
                        <label className="radio" id="A" >
                          {"A. "}
                          <input
                            type="radio"
                            checked={checkedA}
                            name="options"
                            defaultValue="brazil"
                            onClick={(e) => { handleChange('A') }}
                          />{" "}
                          <span className="spanClass">{questions[currentQuestion].optionA.option}</span>
                        </label>
                      </div>
                      <div className="ans ml-2">
                        <label className="radio" id="B">
                          {"B. "}
                          <input
                              checked={checkedB}
                            type="radio"
                            name="options"
                            defaultValue="Germany"
                            onClick={(e) => { handleChange('B') }}
                          />{" "}
                          <span className="spanClass">{questions[currentQuestion].optionB.option}</span>
                        </label>
                      </div>
                      <div className="ans ml-2">
                        <label className="radio" id="C">
                          {"C. "}
                          <input
                            
                            type="radio"
                            checked={checkedC}
                            name="options"
                            defaultValue="Indonesia"
                            onClick={(e) => { handleChange('C') }}
                          />{" "}
                          <span className="spanClass">{questions[currentQuestion].optionC.option}</span>
                        </label>
                      </div>
                      <div className="ans ml-2">
                        <label className="radio" id="D">
                          {"D. "}
                          <input
                           checked={checkedD}
                            type="radio"
                            name="options"
                            defaultValue="Russia"
                            onClick={(e) => { handleChange('D') }}
                          />{" "}
                          <span className="spanClass">{questions[currentQuestion].optionD.option}</span>
                        </label>
                      </div>
                    </div>

                  </div>

                </>
              ) : (
                <></>
              )}


              <div className="d-flex flex-row justify-content-between align-items-center p-3 bg-white">
                <button
                  className="btn btn-primary d-flex align-items-center btn-danger"
                  type="button"
                  style={{ display: "none" }}
                  onClick={() => {
                    handlePreviousButtonClick();
                  }}
                >
                  <i className="fa fa-angle-left mt-1 mr-1" />
                  &nbsp;previous
                </button>
                <button
                  className="btn btn-primary border-success align-items-center btn-success"
                  type="button"
                  id="questionNextButton"
                  onClick={() => handleNextButtonClick()}
                >
                  Next
                  <i className="fa fa-angle-right ml-2" />
                </button>
                <button
                  className="btn btn-primary border-success align-items-center btn-success"
                  type="button"
                  id="questionSubmitButton"
                  style={{ display: "none" }}
                  onClick={() => handleSubmitButtonClick()}
                >
                  Submit
                  <i className="fa fa-angle-right ml-2" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
}

export default Questions;
