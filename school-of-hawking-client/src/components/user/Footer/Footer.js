import React from 'react'

function Footer() {
    return (
        <footer className="footer-1">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="cta-wrapper" style={{justifyContent:"space-evenly",alignItems:"center"}}>
                <img src="assets/images/home/2.png" alt="" />
                <h4 style={{fontFamily:"Lemon, cursive"}}>You can be your own Guiding star<br></br> with our help!</h4>
                {/* <a href="#" className="bisylms-btn">Get Started Now</a> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4 col-md-3">
              <aside className="widget">
                <div className="about-widget">
                  <a href="/"><img  src="/assets/skill2fly/logo/logo1.png"alt="" width="95px" height="85px" /></a>
              
              <h5 style={{fontFamily:"Lemon,cursive"}}>SKILL2FLY.COM</h5>  
                  <p style={{textTransform: " capitalize;"}}>
                  The Future Belongs To Those Enthusiastic<br></br> Who Learn And Adapt New Skills And Change Their Future Forever
                  </p>
                  <div className="ab-social">
                    <a className="fac"><i className="social_facebook" /></a>
                    <a className="twi"><i className="social_twitter" /></a>
                    <a className="you"><i className="social_youtube" /></a>
                    <a className="lin"><i className="social_linkedin" /></a>
                  </div>
                </div>
              </aside>
            </div>
            <div className="col-lg-3 col-md-3">
              <aside className="widget">
                <h3 className="widget-title">Explore</h3>
                <ul>
                  <li><a href="https://www.schoolofhawking.com/#aim" target="_blank">About Us</a></li>
         
                </ul>
              </aside>
            </div>
            <div className="col-lg-3 col-md-3">
              <aside className="widget">
                <h3 className="widget-title">Other</h3>
                <ul>
                  <li><a href="/terms-and-conditions">Terms and Conditions</a></li>
                  <li><a href="/privacy-policy">Cookie Policy</a></li>
                  <li><a href="/refund-policy">Refund and return</a></li>
                 
                </ul>
              </aside>
            </div>
            <div className="col-lg-2 col-md-3">
              <aside className="widget">
                <h3 className="widget-title">Support</h3>
                <ul>
                  <li><a href="/contact">contact us</a></li>
                 
                </ul>
              </aside>
            </div>
          </div>
          {/* Copyrigh */}
          <div className="row">
            <div className="col-lg-12 text-center">
              <div className="copyright" style={{color:"black"}}>
                <p style={{color:"black"}}>© 2022 Copyright all Right Reserved  by <a  style={{color:"black",fontWeight:"bold"}}>Skill2fly</a></p>
              </div>
            </div>
          </div>
          {/* Copyrigh */}
        </div>
      </footer>
    )
}

export default Footer
